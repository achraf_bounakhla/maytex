<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/cdn/buttons.bootstrap4.min.css') ?>">
    <link href="<?php echo base_url("assets/css/main.css"); ?>" rel="stylesheet">

    <style>
        h1 h2 h3 h4 h5 h6 {
            color: #768399;
        }
        table, td, th {
            border: none;
            text-align: left;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {

        }

        #trright {
            width: 35%;
            border: 1px solid black;
        }
        .tablewidth{
            width: 50%;
        }
        .tblack{
            border: solid 1px #5a6268;
            background-color: black;
            color: white;
        }
        .tdblack{
           border: solid 1px #5a6268;
        }
        .vide{
            width: 1000px;
            height: 50px;
            padding: 15px;
        }
        .center{
          text-align: center;
        }
        .left{
            text-align: left;
            padding: 10px;
        }
        .right{
            text-align: right;
            padding: 10px;
        }
        .brtr{
            padding: 10px;
            border-left: solid 1px #768399;
            border-right: solid 1px #768399;
        }


    </style>

</head>

<body style="background: none; width: 80%; margin: auto;color:#768399;">


<table id="tablebordernon">
    <tr>
        <td align="center">
            <div class="page-title">
                <div class="title_center">
                    <h3><font color="black">Facture</font></h3>
                </div>
                <div class="title_left">
                </div>
            </div>
        </td>
    </tr>
</table>

<div class="vide"></div>

<div style="position: absolute;right: 20px;">
<table id="trright">
    <tr>
        <td style="border-right-color: white;height:150px;padding: 10px;"><font color="black">Client :</font></td>
        <td style="border-left-color: white;padding: 10px;"><?php echo $invoice['customer'];?></td>
    </tr>
</table>
</div>

<div class="vide"></div>
<div class="vide"></div>

<table class="tablewidth" id="tablebordernon">

    <tr>
        <td><font color="black">Facture N° :</font></td>
        <td><?php echo $invoice['number'];?></td>
    </tr>

    <tr>
        <td><font color="black">Date :</font></td>
        <td><?php echo $invoice['date']; ?></td>
    </tr>

</table>

<div style="height: 20px"></div>

<div class="col-md-12 col-sm-12 col-xs-12">
    <table class="table" >
        <thead class="table-dark">

        <tr>
            <!--<th>Id</th>-->
            <th class="tblack center">Code</th>
            <th class="tblack left">Article</th>
            <th class="tblack center">Qté</th>
            <th class="tblack center">Unité</th>
            <th class="tblack center">VALEUR (DH)</th>

        </tr>
        </thead>
        <tbody>
        <?php foreach ($invoice['products']  as $row) { ?>
            <tr>

                <td class="center brtr"><?php echo $row['importation_product']['commodityCode']; ?></td>
                <td class="left brtr"><?php echo $row['importation_product']['designation']; ?></td>
                <td class="right brtr"><?php echo $row['quantity']; ?></td>
                <td class="center brtr"><?php echo $row['importation_product']['unit']; ?></td>
                <td class="right brtr"><?php echo $row['net_value']; ?></td>

            </tr>
        <?php } ?>

        <tr>
            <td class="center tdblack" colspan="2" style="border-left-color: white;border-bottom-color: white;"></td>
            <td class="center tdblack" colspan="2"><b>TOTAL</b></td>
            <td class="right tdblack"><b><?php echo $invoice['total']; ?></b></td>
        </tr>

        </tbody>
    </table>
</div> <!-- /col -->

<div style="position: absolute;bottom: 100px;left: 20px;">
    <b>FACTURE SANS PAIEMENT</b>
</div>

</body>
</html>

