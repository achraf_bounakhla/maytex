<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/cdn/buttons.bootstrap4.min.css') ?>">
    <link href="<?php echo base_url("assets/css/main.css"); ?>" rel="stylesheet">

    <style>

        h1 h2 h3 h4 h5 h6 {
            color: #768399;
        }
        table, td, th {
            border: none;
            text-align: left;
            font-size:14px;
        }
        table {
            border-collapse: collapse;
        }
        .tblack{
            border: solid 1px #5a6268;
            background-color: black;
            color: white;
        }
        .tdblack{
            border: solid 1px #5a6268;
        }
        .brtr{
            padding: 10px;
            border-left: solid 1px #768399;
            border-right: solid 1px #768399;
        }
       .vide{
            width: 1000px;
            height: 50px;
            padding: 15px;
        }
        .center{
          text-align: center;
          padding-left: 5px;
          padding-bottom: 1px;
        }
        .left{
            text-align: left;
            padding-left: 5px;
            padding-bottom: 1px;
        }
        .right{
            text-align: right;
            padding-right: 5px;
            padding-bottom: 1px;
        }
    </style>

</head>

<body style="background: none; width: 80%; margin: auto;">



<div class="page-title">
    <div class="title_center" style="text-align: center";>
        <h3><font color="black"><u>ETAT D'APUREMENT & DECHARGES</u></font></h3>
    </div>
</div>

                                                                            <div class="vide"></div>

<div class="col-md-12 col-sm-12 col-xs-12">
    <table class="table" >
        <thead class="table-dark">

        <tr>
            <!--<th>Id</th>-->
            <th class="tblack center">N°Dossier</th>
            <th class="tblack center">DATE</th>
            <th class="tblack center">ORIGINE</th>
            <th class="tblack center">Code</th>
            <th class="tblack left">DESIGNATION</th>
            <th class="tblack right">QUANTITE</th>
            <th class="tblack center">UNITE</th>
            <th class="tblack right">VALEUR BRUT</th>
            <th class="tblack right">VALEUR NET</th>
            <th class="tblack right">POIDS NET</th>

        </tr>
        </thead>
        <tbody>
        <?php foreach ($clearance['clearances']  as $row) { ?>
            <tr>

                <td class="center brtr"><?php echo $row['DUM']; ?></td>
                <td class="center brtr"><?php echo $row['date']; ?></td>
                <td class="center brtr"><?php echo $row['country']; ?></td>
                <td class="center brtr"><?php echo $row['commodityCode']; ?></td>
                <td class="left brtr"><?php echo $row['designation']; ?></td>
                <td class="right brtr"><?php echo $row['quantity']; ?></td>
                <td class="center brtr"><?php echo $row['unit']; ?></td>
                <td class="right brtr"><?php echo $row['brute_value']; ?></td>
                <td class="right brtr"><?php echo $row['net_value']; ?></td>
                <td class="right brtr"><?php echo $row['net_weight']; ?></td>

            </tr>
        <?php } ?>

        <tr>
            <td class="center tdblack" colspan="5" style="border-left-color: white;border-bottom-color: white;"></td>
            <td class="right tdblack"><b><?php echo $clearance['totalQuantity']; ?></b></td>
            <td class="center tdblack"></td>
            <td class="right tdblack"><b><?php echo $clearance['totalBruteValue']; ?></b></td>
            <td class="right tdblack"><b><?php echo $clearance['totalNetValue']; ?></b></td>
            <td class="right tdblack"><b><?php echo $clearance['totalNetWeight']; ?></b></td>
        </tr>

        </tbody>
    </table>
</div> <!-- /col -->

</body>
</html>

