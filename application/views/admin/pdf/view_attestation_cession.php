<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/cdn/buttons.bootstrap4.min.css') ?>">
    <link href="<?php echo base_url("assets/css/main.css"); ?>" rel="stylesheet">

    <style>
        h1 h2 h3 h4 h5 h6 {
            color: #768399;
        }
        table, td, th {
            border: none;
            text-align: left;
        }
        table {
            border-collapse: collapse;
            width:40%;
        }
       .vide{
            width: 1000px;
            height: 50px;
            padding: 15px;
        }
        .center{
          text-align: center;
        }
        .left{
            text-align: left;
            padding: 5px;
        }
        .right{
            text-align: right;
            padding: 5px;
        }
        .divAT{
             margin-top: 20px;
             margin-bottom: 20px;
        }
        .divCustomer{
            margin-top: 30px;
        }
    </style>

</head>

<body style="background: none; width: 80%; margin: auto;">

                                                                            <div class="vide"></div>

<div class="page-title">
    <div class="title_center" style="text-align: center";>
        <h3><font color="black"><u>ATTESTATION DE CESSION</u></font></h3>
    </div>
</div>


<div class="divCustomer">
    <P> Nous Soussingnons Societé MAYTEX  Avoir Cédé à la <?php echo $cession['customer']['name'];?> Demeurant à <?php echo $cession['customer']['address'];?> .</P>
</div>

                                                                            <div class="vide"></div>

<div class="col-md-12 col-sm-12 col-xs-12">
    <table class="table" >
        <tbody>
        <?php foreach ($cession['products']  as $row) { ?>
            <tr>

                <td class="left"><?php echo $row['importation_product']['designation']; ?></td>
                <td class="right"><?php echo $row['quantity']; ?></td>
                <td class="left"><?php echo $row['importation_product']['unit']; ?></td>

            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

                                                                            <div class="vide"></div>

<div class="divAT">
    <b>En décharge d'AT suivantes :</b>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
    <table>
        <tbody>
        <?php foreach ($cession['importations']  as $row) { ?>
            <tr>
                <td>
                    <?php echo $row['DUM']; ?> du <?php echo $row['date']; ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

</body>
</html>

