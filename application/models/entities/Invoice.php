<?php



class Invoice {
    protected $number;
    protected $date;
    protected $customer; //Customer
    protected $products = array(); //Clearance
    protected $total;

    /**
     *
     */
    public function init(){
        $invoice = array(
            'number'=>1,
            'date'=>'16-11-2021',
            'customer'=>'KHALID ESSALHI',
            'products'=>array(
                array(
                    'net_value'=>10468.56,
                    'quantity'=>'1614.00',
                    'importation_product'=>array(
                        'commodityCode'=>3999999999,
                        'designation'=>'EVA',
                        'unit'=>'M2',
                    )
                ),
                array(
                    'net_value'=>2505.992,
                    'quantity'=>'427.00',
                    'importation_product'=>array(
                        'commodityCode'=>60010000000,
                        'designation'=>'GALAXY',
                        'unit'=>'M2',
                    )
                ),
                array(
                    'net_value'=>1198.783,
                    'quantity'=>'252.00',
                    'importation_product'=>array(
                        'commodityCode'=>60010000000,
                        'designation'=>'GLOVEX',
                        'unit'=>'M2',
                    )
                ),
                array(
                    'net_value'=>29019.421,
                    'quantity'=>'1534.00',
                    'importation_product'=>array(
                        'commodityCode'=>60010000000,
                        'designation'=>'JAGUAR',
                        'unit'=>'M2',
                    )
                ),
                array(
                    'net_value'=>11730.899,
                    'quantity'=>'2713.00',
                    'importation_product'=>array(
                        'commodityCode'=>60010000000,
                        'designation'=>'NON TISSU',
                        'unit'=>'M2',
                    )
                ),
                array(
                    'net_value'=>11745.800,
                    'quantity'=>'562.00',
                    'importation_product'=>array(
                        'commodityCode'=>60010000000,
                        'designation'=>'SARJA',
                        'unit'=>'M2',
                    )
                ),
                array(
                    'net_value'=>2209.211,
                    'quantity'=>'78.00',
                    'importation_product'=>array(
                        'commodityCode'=>60010000000,
                        'designation'=>'TEKNOFIL',
                        'unit'=>'M2',
                    )
                ),
                array(
                    'net_value'=>14911.998,
                    'quantity'=>'2323.00',
                    'importation_product'=>array(
                        'commodityCode'=>60010000000,
                        'designation'=>'AIRNET',
                        'unit'=>'M2',
                    )
                )
            ),

            'total'=>83790.661,
        );


        return $invoice;

    }
}
