<?php
class Cession
{
    protected $customer = array(); //Customer
    protected $products = array(); //Product
    protected $importations = array(); //Importation

    public function init()
    {
        $cession = array(
            'customer' =>array(
                'name' => 'Société ANIMAC',
                'address' => 'Route 110 Sidi Bernoussi Q.I lot N° 4 Casablanca',
            ),
            'products' => array(
                array(
                    'quantity' => '1614.00',
                    'importation_product' => array(
                        'designation' => 'EVA',
                        'unit' => 'M2',
                    )
                ),
                array(
                    'quantity'=>'427.00',
                    'importation_product'=>array(
                        'designation'=>'GALAXY',
                        'unit'=>'M2',
                    )
                ),
                array(
                    'quantity'=>'252.00',
                    'importation_product'=>array(
                        'designation'=>'GLOVEX',
                        'unit'=>'M2',
                    )
                ),
                array(
                    'quantity'=>'1534.00',
                    'importation_product'=>array(
                        'designation'=>'JAGUAR',
                        'unit'=>'M2',
                    )
                ),
                array(
                    'quantity'=>'2713.00',
                    'importation_product'=>array(
                        'designation'=>'NON TISSU',
                        'unit'=>'M2',
                    )
                ),
                array(
                    'quantity'=>'562.00',
                    'importation_product'=>array(
                        'designation'=>'SARJA',
                        'unit'=>'M',
                    )
                ),
                array(
                    'quantity'=>'78.00',
                    'importation_product'=>array(
                        'designation'=>'TEKNOFIL',
                        'unit'=>'M2',
                    )
                ),
                array(
                    'quantity'=>'2323.00',
                    'importation_product'=>array(
                        'designation'=>'AIRNET',
                        'unit'=>'M2',
                    )
                )
            ),
            'importations' =>array(
                array(
                    'DUM' => '302 037 2019 000586 D',
                    'date' => '31-07-19',
                ),
                array(
                    'DUM' => '300 037 2019 000177 J',
                    'date' => '15-10-19',
                )
            ),
        );

        return $cession;

    }



}