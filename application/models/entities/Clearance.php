<?php

class Clearance{

    protected $importation_product; //ImportationProdct
    protected $net_value;
    protected $quantity;

    public function init(){
        $clearanceState = array(
            'clearances'=> array(
                0 => array(
                    'DUM'=>'300 037 2019 000589 D',
                    'date'=>'31-07-19',
                    'country'=>'Europe',
                    'commodityCode'=>'3921909890',
                    'designation'=>'JAGUAR',
                    'quantity'=>'1534.000',
                    'unit'=>'M2',
                    'net_value'=>'29019.421',
                    'brute_value'=>'29019.421',
                    'net_weight'=>'942.335',
                ),
                1 => array(
                    'DUM'=>'300 037 2019 000589 D',
                    'date'=>'31-07-19',
                    'country'=>'Europe',
                    'commodityCode'=>'3921909890',
                    'designation'=>'NON TISSU',
                    'quantity'=>'1534.000',
                    'unit'=>'M2',
                    'net_value'=>'29019.421',
                    'brute_value'=>'29019.421',
                    'net_weight'=>'942.335',
                ),
                2 => array(
                    'DUM'=>'300 037 2019 000589 D',
                    'date'=>'31-07-19',
                    'country'=>'Europe',
                    'commodityCode'=>'3921909890',
                    'designation'=>'EVA',
                    'quantity'=>'1534.000',
                    'unit'=>'M2',
                    'net_value'=>'29019.421',
                    'brute_value'=>'29019.421',
                    'net_weight'=>'942.335',
                ),
                3 => array(
                    'DUM'=>'300 037 2019 000589 D',
                    'date'=>'31-07-19',
                    'country'=>'Europe',
                    'commodityCode'=>'3921909890',
                    'designation'=>'GLOVEX',
                    'quantity'=>'1534.000',
                    'unit'=>'M2',
                    'net_value'=>'29019.421',
                    'brute_value'=>'29019.421',
                    'net_weight'=>'942.335',
                ),
                4 => array(
                    'DUM'=>'300 037 2019 000589 D',
                    'date'=>'31-07-19',
                    'country'=>'Europe',
                    'commodityCode'=>'3921909890',
                    'designation'=>'AIRNET',
                    'quantity'=>'1534.000',
                    'unit'=>'M2',
                    'net_value'=>'29019.421',
                    'brute_value'=>'29019.421',
                    'net_weight'=>'942.335',
                ),
                5 => array(
                    'DUM'=>'300 037 2019 000589 D',
                    'date'=>'31-07-19',
                    'country'=>'Europe',
                    'commodityCode'=>'3921909890',
                    'designation'=>'GALAXY',
                    'quantity'=>'1534.000',
                    'unit'=>'M2',
                    'net_value'=>'29019.421',
                    'brute_value'=>'29019.421',
                    'net_weight'=>'942.335',
                ),
                6 => array(
                    'DUM'=>'300 037 2019 000177 J',
                    'date'=>'31-07-19',
                    'country'=>'Europe',
                    'commodityCode'=>'3921909890',
                    'designation'=>'AIRNET',
                    'quantity'=>'1534.000',
                    'unit'=>'M2',
                    'net_value'=>'29019.421',
                    'brute_value'=>'29019.421',
                    'net_weight'=>'942.335',
                ),
                7 => array(
                    'DUM'=>'300 037 2019 000177 J',
                    'date'=>'31-07-19',
                    'country'=>'Europe',
                    'commodityCode'=>'3921909890',
                    'designation'=>'SARJA',
                    'quantity'=>'1534.000',
                    'unit'=>'M2',
                    'net_value'=>'29019.421',
                    'brute_value'=>'29019.421',
                    'net_weight'=>'942.335',
                ),
                8 => array(
                    'DUM'=>'300 037 2019 000177 J',
                    'date'=>'31-07-19',
                    'country'=>'Europe',
                    'commodityCode'=>'3921909890',
                    'designation'=>'TEKNOFIL',
                    'quantity'=>'1534.000',
                    'unit'=>'M2',
                    'net_value'=>'29019.421',
                    'brute_value'=>'29019.421',
                    'net_weight'=>'942.335',
                )
            ),

            'totalQuantity'=>'9503.000',
            'totalBruteValue'=>'83790.661',
            'totalNetValue'=>'83790.661',
            'totalNetWeight'=>'3020.901',
        );

        return $clearanceState;
    }
}
