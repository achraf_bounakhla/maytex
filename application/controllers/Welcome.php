<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}
    public function pdf()
    {

        $this->load->model('entities/invoice');
        $invoice=$this->invoice->init();
        $data['invoice'] = $invoice;
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $html = $this->load->view('admin/pdf/view_cmd_article',$data,true);
        $pdf->WriteHTML($html);
        $output = 'itemreport' . date('Y_m_d_H_i_s') . '_.pdf';
        $pdf->Output("$output", 'I');

    }
    public function pdfCession()
    {
        $this->load->model('entities/cession');
        $cession=$this->cession->init();
        $data['cession'] = $cession;
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $html = $this->load->view('admin/pdf/view_attestation_cession',$data,true);
        $pdf->WriteHTML($html);
        $output = 'itemreport' . date('Y_m_d_H_i_s') . '_.pdf';
        $pdf->Output("$output", 'I');
    }
    public function pdfApurement()
    {
        $this->load->model('entities/clearance');
        $clearance=$this->clearance->init();
        $data['clearance'] = $clearance;
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf->AddPage('L');
        $html = $this->load->view('admin/pdf/view_apurement_decharges',$data,true);
        $pdf->WriteHTML($html);
        $output = 'itemreport' . date('Y_m_d_H_i_s') . '_.pdf';
        $pdf->Output("$output", 'I');

    }
}
